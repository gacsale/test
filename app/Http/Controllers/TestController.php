<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class TestController extends Controller
{


    public function showForm()
    {
        return view('test');
    }

    public function postForm(Request $request)
    {
        // dd('test');
        $nit = $request->input('nit');
        $xml = '<Servicio>ValidaNIT</Servicio><Usuario>PRICESMARTWS</Usuario><Clave>FELPRICE2020WS</Clave><Nit>' . $nit . '</Nit>';
        $resource = 'app/gf/fel/dte/ServiciosFEL';

        $client = new Client([
            'base_uri' => 'https://dte.guatefacturas.com/',
            'headers' => [
                'Content-Type' => 'text/xml',
                'PARAMETROS' => '<Servicio>ValidaNIT</Servicio><Usuario>PRICESMARTWS</Usuario><Clave>FELPRICE2020WS</Clave><Nit>19</Nit>'
            ],
            'body'   => $xml,
            'timeout'  => 30.0,
            'verify' => false,
        ]);

        $response = $client->request('POST', $resource, ['auth' => ['WsFelGuatefac', 'FelW52020.gt']] );
        $contents = $response->getBody()->getContents();

        dd($contents);


    }



}
